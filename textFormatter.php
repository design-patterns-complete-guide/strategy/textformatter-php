<?php
interface TextFormatter {
    public function format($text);
}
class LowercaseFormatter implements TextFormatter {
    public function format($text) {
        return strtolower($text);
    }
}
class UppercaseFormatter implements TextFormatter {
    public function format($text) {
        return strtoupper($text);
    }
}
class CamelCaseFormatter implements TextFormatter {
    public function format($text) {
        $words = explode(' ', $text);
        $camelCaseText = '';
        foreach ($words as $word) {
            $camelCaseText .= ucfirst(strtolower($word));
        }
        return lcfirst($camelCaseText);
    }
}
class TextProcessor {
    private $textFormatter;
    public function __construct(TextFormatter $textFormatter) {
        $this->textFormatter = $textFormatter;
    }
    public function processText(string $text) {
        return $this->textFormatter->format($text);
    }
}
// Client code
$text = "Hello, World! This is a Text Formatting Example.";
// Choose the text formatting strategy at runtime
$chosenTextFormatter = new CamelCaseFormatter(); // You can change this to Lowercase or Uppercase
$textProcessor = new TextProcessor($chosenTextFormatter);
$formattedText = $textProcessor->processText($text);
echo "Formatted text: $formattedText";